import { Component, OnInit } from '@angular/core';
import { Article } from '../entities';
import { ArticleService } from '../article.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  // Attribut qui va faire le lien avec le champ de formulaire
  titreFilter?:string;
  // Liste d'opérations pour le retour de la requête
  list?:Article[] = [];
  // Booléen pour gérer l'affichage de la liste de résultats
  displayResultList:boolean = false;


  constructor(private arService:ArticleService) {
    this.titreFilter = "";
   }

  ngOnInit(): void {
  }

  searchByTitre() {
    if (this.titreFilter?.length == 0) {
      alert("Merci de saisir une valeur !");
      document.getElementById('titreFilter')?.focus();
    } else {
      this.arService.search(this.titreFilter!).subscribe(
        data => this.list = data   
      );
      this.displayResultList = true;
    }
  }
}
