import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Article } from '../entities';
import { ArticleService } from '../article.service';
@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  article:Article = {
    contenu: '',
    titre: '',
  };
  constructor(private arService:ArticleService, private router:Router) { }

  ngOnInit(): void {
  }

  addArticle() {
    this.arService.add(this.article).subscribe(() => {
      this.router.navigate(['/']);
    }); //On oublie pas le subscribe, sinon la requête ne sera pas lancée
  }
}
