import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ArticleService } from '../article.service';
import { Article } from '../entities';


@Component({
  selector: 'app-single-article',
  templateUrl: './single-article.component.html',
  styleUrls: ['./single-article.component.css']
})
export class SingleArticleComponent implements OnInit {

  constructor(private route:ActivatedRoute, private service:ArticleService) { }

  article?:Article;
  routeId?:string;
  ngOnInit(): void {
    this.route.params.subscribe(param => {
      this.routeId = param['id'];
      console.log(this.routeId);
      this.service.getById(Number(this.routeId!)).subscribe(data=>this.article=data);
      
      
    });

  }

}
