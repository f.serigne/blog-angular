export interface  Article{
    id?: number;
    titre?: string;
    date?: Date
    contenu: string;
}
