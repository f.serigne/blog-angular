import { Component, OnInit } from '@angular/core';
import { Article } from '../entities';
import { ArticleService } from '../article.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  articles:Article[] = []
  single?:Article;
  displayEditForm:boolean = false;
  modifiedar:Article = {titre: "", contenu:"" };
  modifiedAr: any;
  constructor(private arService:ArticleService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.arService.getAll().subscribe(data => this.articles = data);
  }

  fetchOne() {
    this.arService.getById(2).subscribe(data => this.single = data);
  }

  delete(id:number) {
    this.arService.delete(id).subscribe();
    this.removeFromList(id);
  }

  removeFromList(id: number) {
    this.articles.forEach((article, article_index) => {
      if(article.id == id) {
        this.articles.splice(article_index, 1);
      }
    });
  }

  fetchAll(){
    this.arService.getAll().subscribe(data => this.articles = data);
  }

  showEditForm(article:Article) {
    this.displayEditForm = true;
    // Impossible, le = copie juste la référence, il ne fait pas une copie
    // this.modifiedar = article;
    this.modifiedAr = Object.assign({},article);
  }

  update(article:Article): void {
    // Envoyer au serveur
    this.arService.put(article).subscribe();
    // Mettre à jour la liste en HTML
    this.updateArticleInList(article);
    // Fermer le formulaire
    this.displayEditForm = false;
  }

  updateArticleInList(article:Article) {
    this.articles.forEach((ar_list) => {
      if(ar_list.id == article.id) {
        ar_list.contenu = article.contenu;
        ar_list.date = article.date;
        ar_list.titre = article.titre;
      }
    });
  }

}
