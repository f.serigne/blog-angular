import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddComponent } from './add/add.component';
import { ArticleComponent } from './article/article.component';
import { SearchComponent } from './search/search.component';
import { SingleArticleComponent } from './single-article/single-article.component';

const routes: Routes = [
  {path: '', component: ArticleComponent}, //définir la page d'accueil de l'appli
  {path: 'search', component: SearchComponent}, //page de recherche
  {path: 'add-article', component: AddComponent},
  {path:'single-article/:id',component:SingleArticleComponent} //http://localhost:4200/add-operation
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
